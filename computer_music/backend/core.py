import random
import math
import utils
import rules
import json
import logging

LOGGER = logging.getLogger(__name__)

BPM_MIN = 40
BPM_MAX = 180
BPM_STEP = 5



'''

track

{
name: str
bpm: int
hit: int (0 or 1)
key: int (1 to 24)
}

'''


class Playlist(object):
    def __init__(self, tracks):
        self.bpm_min = BPM_MIN
        self.bpm_max = BPM_MAX
        self.set_bpm_min_max(tracks)
        self.buckets = []
        self.init_buckets()
        self.distribute(tracks)
        self.stats = {}
        self.metadata = {}

    @staticmethod
    def from_text_file(path):
        pass

    @staticmethod
    def from_json(j):
        pass

    def tracks_to_csv(self):
        all = self.join_buckets()
        rows = map(utils.dict_to_csv, all.tracks)
        rows.insert(0, ','.join(map(str, (sorted(all[0].keys())))))
        return '\n'.join(rows)

    def to_json(self):
        d = {
            'stats': self.stats,
            'playlist': self.join_buckets().tracks,
            'metadata': self.metadata
        }
        return json.dumps(d, indent=1)

    def join_buckets(self, idx=None):
        if not idx:
            b = self.buckets
        else:
            b = map(self.buckets.__getitem__, idx)

        return reduce(Bucket.join, b)

    def swap(self, a, b):
        if isinstance(a, Bucket):
            a = self.buckets.index(a)

        if isinstance(b, Bucket):
            b = self.buckets.index(b)

        tmp = self.buckets[a]
        self.buckets[a] = self.buckets[b]
        self.buckets[b] = tmp

    def set_bpm_min_max(self, tracks):
        self.bpm_min = min(tracks, key=lambda x: x['bpm'])['bpm']
        self.bpm_max = max(tracks, key=lambda x: x['bpm'])['bpm'] + 1

    def init_buckets(self):
        _range = self.bpm_max - self.bpm_min

        if _range < 0:
            raise PlaylistException('Max bpm can not be less then Min bpm')

        if BPM_STEP > _range:
            num_of_buckets = 1
        else:
            num_of_buckets = int(math.ceil((_range) / float(BPM_STEP)))

        low = self.bpm_min
        high = self.bpm_min + BPM_STEP
        for i in range(0, num_of_buckets):
            self.buckets.append(
                Bucket(
                    low=low,
                    high=high
                )
            )
            low += BPM_STEP
            high += BPM_STEP

    def distribute(self, tracks):
        for track in tracks:
            idx = int((track['bpm'] - self.bpm_min) / BPM_STEP)
            try:
                b = self.buckets[idx]
            except IndexError:
                raise PlaylistException('index {} is out of range {}, track.bpm = {}'.format(idx, self.len_of_buckets(), track['bpm']))
            b.add(track)

    def apply_rule(self, func, idx_list=None):
        if not idx_list:
            for bucket in self.buckets:
                bucket.apply_rule(func)
        else:
            if max(idx_list) >= self.buckets or min(idx_list) < 0:
                raise IndexError(self.apply_rule.__name__)
            for i in idx_list:
                self.buckets[i].apply_rule(func)

    def apply_rules(self, rules):
        LOGGER.debug(rules)
        for rule in rules:
            self.apply_rule(rule)

    def len_of_buckets(self):
        return len(self.buckets)

    def __len__(self):
        return sum(map(len, self.buckets))

    def __str__(self):
        out = []
        low = self.bpm_min
        for bucket in self.buckets:
            high = low + BPM_STEP
            line = '{}-{}: {}'.format(
                low,
                high,
                bucket
            )
            out.append(line)
            low += BPM_STEP

        return '\n'.join(out)


class Bucket(object):
    def __init__(self, low, high, tracks=None):
        if tracks:
            self.tracks = tracks
        else:
            self.tracks = []

        self.low = low
        self.high = high

    def add(self, track):
        if isinstance(track, list) or isinstance(track, set):
            self.extend(track)
        else:
            self.tracks.append(track)

    def join(self, bucket):
        new_low = min(self.low, bucket.low)
        new_high = max(self.high, bucket.high)

        new_lst = self.tracks[:]
        new_lst.extend(bucket.tracks)

        return Bucket(low=new_low, high=new_high, tracks=new_lst)

    def apply_rule(self, func):
        '''
        func should return a new list
        :param func:
        :return:
        '''
        self.tracks = func(self.tracks)

    def __len__(self):
        return len(self.tracks)

    def __iter__(self):
        return iter(self.tracks)

    def __str__(self):
        return ', '.join([str(track) for track in self .tracks])

    def __getitem__(self, item):
        return self.tracks[item]


class PlaylistException(Exception):
    pass

if __name__ == '__main__':
    d = BPM_MAX - BPM_MIN
    tracks = [
        {
            'name': utils.get_random_name(),
            'bpm': int(math.floor(random.random() * d + BPM_MIN)),
            'key': int(math.floor(random.random() * 23 + 1)),
            'hit': utils.flip_unfair_coin(0.6)
        }
        for i in range(0, 10)
    ]
    # print(tracks)
    playlist = Playlist(tracks)
    playlist.apply_rule(rules.sort_by_key)
    playlist.apply_rule(rules.sort_by_bpm)
    print len(playlist)
    playlist.apply_rule(rules.spread_hits_evenly)
    print playlist.tracks_to_csv()
    print len(playlist)
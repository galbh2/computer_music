import core
import rules


def get_rules():
    return str(rules.RULES)


def build_playlist(j, out_format='json'):
    try:
        _rules = j['rules']
        tracks = j['playlist']
    except:
        raise InterfaceError('Malformed json, rules or playlist sections are missing')

    parsed_rules = _parse_rules(_rules)
    playlist = core.Playlist(tracks)
    playlist.apply_rules(parsed_rules)

    if out_format == 'json':
        return playlist.to_json()
    if out_format == 'csv':
        return playlist.tracks_to_csv()


def _parse_rules(rules_to_parse):
    l = []
    for rule in rules_to_parse:
        try:
            l.append(getattr(rules, rule))
        except AttributeError:
            raise InterfaceError('Rule {} does not exist'.format(rule))

    return l


class InterfaceError(Exception):
    pass

if __name__ == '__main__':
    l = ['sort_by_bpm']
    print _parse_rules(l)



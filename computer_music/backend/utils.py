import random
import string
import math


def even_split(lst, n=None):
    size = len(lst)
    if n is None or n > size:
        n = size

    splits = []
    step_size = int(size / n)
    remainder = size % n

    start = 0
    end = step_size + 1
    for i in range(0, remainder):
        splits.append(lst[start:end])
        start = end
        end += step_size + 1

    end -= 1

    for i in range(0, n - remainder):
        splits.append(lst[start:end])
        start = end
        end += step_size

    return n, splits


def zigzag(a, b):
    new_lst = []
    counter = 0

    def add(what, to_what):
        if isinstance(what, list) or isinstance(what, set):
            to_what.extend(what)
        else:
            to_what.append(what)

    while a and b:
        if counter % 2:
            add(a.pop(0), new_lst)
        else:
            add(b.pop(0), new_lst)
        counter += 1

    if not a:
        for i in b:
            add(i, new_lst)
    elif not b:
        for i in a:
            add(i, new_lst)

    return new_lst


def get_random_name(length=5):
    return ''.join([random.choice(string.letters) for i in range(length)])


def flip_unfair_coin(mode):
    r = random.triangular(low=0, high=1, mode=mode)
    if r <= mode:
        return 0
    else:
        return 1


def dict_to_csv(d):
    k, v = zip(*sorted(d.items()))
    return ','.join(map(str, v))

if __name__ == '__main__':
    l = [0, 1, 2, 3, 4, 5, 6 ,7 ,8 ]


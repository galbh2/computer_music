# TODO: turn function to class

import utils
import functools

RULES = {}


def register(func):
    RULES[func.__name__] = func.__doc__

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


@register
def sort_by_key(lst):
    '''
    Sort the tracks by increasing key
    '''
    return sorted(
        lst,
        key=lambda track: track['key']
    )


@register
def sort_by_bpm(lst):
    '''
    Sort the tracks by increasing BPM.
    '''
    return sorted(
        lst,
        key=lambda track: track['bpm']
    )


@register
def sort_by_bpm_reverse(lst):
    '''
    Sort
    '''
    return sorted(
        lst,
        key=lambda track: track['bpm'],
        reverse=True
    )


@register
def spread_hits_evenly(lst):
    hits = []
    others = []

    for track in lst:
        if track['hit']:
            hits.append(track)
        else:
            others.append(track)

    if not hits or not others:
        return lst

    if len(others) > len(hits):
        _, splits = utils.even_split(others, len(hits) + 1)
        return utils.zigzag(splits, hits)
    elif len(others) < len(hits):
        _, splits = utils.even_split(hits, len(others) + 1)
        return utils.zigzag(others, splits)
    else:
        return utils.zigzag(others, hits)





'''
{
key: {mean: val, variance: val},
bpm: {mean: val, variance: val},
hit: {mean: val, variance: val},
}

stats.key.mean
stats.goodness
...

'''


def mean(data):
    size = len(data)
    if not size:
        return 0

    return sum(data) / float(size)


def variance(data, mean=None):
    size = len(data)
    if not size:
        return 0

    if not mean:
        mean = mean(data)

    _sum = 0
    for x in data:
        _sum += (x - mean) ** 2

    return _sum / float(size)


class Index(object):
    def __init__(self, data):
        self.data
        self.key
        self.index = {}

    def calc_all(self):
        for i in self.data:
            self.index['mean'] = mean(self.data)
            self.index['var'] = variance(self.data, self.index['mean'])

    def __getattr__(self, item):
        if item not in self.index:
            raise AttributeError('Attribute {} not found'.format(item))

        return self.index[item]


class Stats(object):
    def __init__(self, data):
        self.stats = {}
        self.data = data
        self.size = len(data)
        self.attributes = {}
        self.set_attributes()

    def set_attributes(self):
        if not self.data:
            return

        instance = self.data[0]
        if not isinstance(instance, dict):
            return

        # keep the keys which their values are numbers
        keys = [k for k in instance.keys() if isinstance(k, (int, long, float))]

        # for each key, build a list of the matching values from all of self.data
        d = {k: list() for k in keys}
        for i in self.data:
            for k in d.keys():
                d[k].append(i[k])

        for k, v in d.values():
            self.attributes[k] = Index(v)

    def __getattr__(self, item):
        if item not in self.attributes:
            raise AttributeError('Attribute {} not found'.format(item))

        return self.attributes[item]


class TransitionStats(Stats):
    def __init__(self, data):
        transitions = []
        for i in range(data - 1):
            transitions.append(
                {
                    'bpm': abs(data[i]['bpm'] - data[i + 1]['bpm']),
                    'key': abs(data[i]['key'] % 12) - data[i + 1]['key'] % 12
                }
            )
        super(TransitionStats, self).__init__(transitions)

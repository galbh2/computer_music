from flask import Flask, render_template, request, abort
from werkzeug.exceptions import BadRequest
from computer_music.backend import interface as music
import json

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/rules')
def rules():
    return music.get_rules()


@app.route('/playlist', methods=['POST'])
def playlist():
    try:
        j = request.get_json(force=True)
        result = music.build_playlist(j)
    except (BadRequest, music.InterfaceError) as e:
        abort(400, e.message)

    return result


@app.route('/playlist_csv', methods=['POST'])
def playlist_csv():
    try:
        j = request.get_json(force=True)
        result = music.build_playlist(j, out_format='csv')
    except (BadRequest, music.InterfaceError) as e:
        abort(400, e.message)

    return result

if __name__ == '__main__':
    app.run(debug=True)

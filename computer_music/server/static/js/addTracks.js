

$(document).ready(function(){

	


	$('#nextArrow').click(function () {

		
var k = kontext( document.querySelector( '.kontext' ) );
  if(k.getIndex() === 1)
  {
  	var jsonObj = fetchData();
  	$.ajax({
	type: 'POST',
// Provide correct Content-Type, so that Flask will know how to process it.
contentType: 'application/json',
// Encode data as JSON.
data: JSON.stringify(jsonObj),
// This is the type of data expected back from the server.
dataType: 'json',
url: '/playlist',
success: function (res) {
	addTracksPost(res);
}
});
  	


  }
  nextStep();
});



	$('#backArrow').click(function () {
		var k = kontext( document.querySelector( '.kontext' ) );
           if(k.getIndex() === 2)
           {
           	clearPostData();
           }

           backStep();
  



	});




});

function addTrack() {

////get data from the inputs
var songName = document.getElementById("songName").value;
var bpm = document.getElementById("bpm").value;
var isHit = document.getElementById("isHit").checked;
var key = document.getElementById("key");
var keyText = key.options[key.selectedIndex].text;


if(songName === "" || bpm === "" || keyText === "key")
{
	alert("illegal inputs \n Pleace try again");
}
else
{

////add row to the table
var identifier = Date.now();
var table = document.getElementById("tab_logic");
var row = table.insertRow(1);

var cell1 = row.insertCell(0);
var cell2 = row.insertCell(1);
var cell3 = row.insertCell(2);
var cell4 = row.insertCell(3);
var cell5 = row.insertCell(4);
cell1.innerHTML = songName;
cell2.innerHTML = bpm;
cell3.innerHTML = isHit ? "yes" : "no"
cell4.innerHTML = keyText;
cell5.innerHTML = "<button class=\"btn btn-danger glyphicon glyphicon-remove row-remove\" onClick=\"deleteRowFromTheTable(this)\" value=\"" + identifier + "\">";

/////delete old data
document.getElementById("songName").value = "";
document.getElementById("bpm").value = "";

}
}

function addTracksPost(playlist) {
////get data from the inputs



var tracks = playlist['playlist'];

////add row to the table
var identifier = Date.now();
var table = document.getElementById("tab_logic_post");
for (var i = 0; i < tracks.length; i++) {
	var row = table.insertRow(i + 1);
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	var cell4 = row.insertCell(3);

	cell1.innerHTML = tracks[i]['name']
	cell2.innerHTML = tracks[i]['bpm']
	cell3.innerHTML = tracks[i]['hit']
	cell4.innerHTML = mapIntToString(tracks[i]['key']);	
}

}

function mapIntToString(key){
	map = {
"0" : "E minor",
"1" : "A minor",
"2" : "D minor",
"3" : "G minor",
"4" : "C minor",
"5" : "F minor",
"6" : "B flat minor",
"7" : "E flat minor",
"8" : "A flat minor",
"9" : "D flat minor",
"10" : "F sharp minor",
"11" : "B minor",
"12" : "G major",
"13" : "C major",
"14" : "F major",
"15" : "B flat major",
"16" : "E flat major",
"17" : "A float major",
"18" : "D flat major",
"19" : "F sharp major",
"20" : "B major",
"21" : "E major",
"22" : "A major",
"23" : "D major"
	}

	return map[key];
}


function mapStringToInt(key){
	map = {
"E minor" : 0,
"A minor" : 1,
"D minor": 2,
"G minor": 3,
"C minor": 4,
"F minor": 5,
"B flat minor": 6,
"E flat minor": 7,
"A flat minor": 8,
"D flat minor": 9,
"F sharp minor": 10,
"B minor": 11,
"G major": 12,
"C major": 13,
"F major": 14,
"B flat major": 15,
"E flat major": 16,
"A float major": 17,
"D flat major": 18,
"F sharp major": 19,
"B major": 20,
"E major": 21,
"A major": 22,
"D major": 23
	}

	return map[key];
}

function findIdxTrack(i_identifier)
{

	var identifier = i_identifier;
	var table = document.getElementById("tab_logic");
	var numOfRows = table.rows.length;

	for (var i = 1; i<numOfRows; i++) {

		if(table.rows[i].cells[4].childNodes[0].value == identifier)
		{
			return i;

		}

	}

}

function deleteRowFromTheTable(row)
{
	var identifier = row.value;
	document.getElementById("tab_logic").deleteRow(findIdxTrack(identifier));

}


function getSelectedRules()
{

	var selectedRulesElements = document
	.getElementById("selected-rules")
	.getElementsByTagName('div');

	var rules = [];

	for(i=0; i< selectedRulesElements.length; i++ )
	{
		rules.push(selectedRulesElements[i].getAttribute('value'));    
	}
	return rules;

}



function getTracksList()
{	 
	var table = document.getElementById("tab_logic");
	var numOfRows = table.rows.length;

	var playList = [];

	for (var i = 1; i<numOfRows; i++) {


		var track = {};

		track.hit = table.rows[i].cells[2].innerHTML === "yes" ? 1 : 0;
		track.bpm = parseInt(table.rows[i].cells[1].innerHTML);
		track.name = table.rows[i].cells[0].innerHTML;
		track.key = mapStringToInt(table.rows[i].cells[3].innerHTML);

		playList.push(track);	 		   
	}

	console.log(playList)

	return playList;
}



function fetchData()
{
	var playlist = getTracksList();
	var rules = getSelectedRules();

	var jsonObj = {};

	jsonObj.playlist = playlist;
	jsonObj.rules = rules;
	return jsonObj;
}


function nextStep()
{
	var k = kontext( document.querySelector( '.kontext' ) );
	if(k.getIndex() === 0)
	{
		document.getElementById("backArrow").style.display = "block";
		document.getElementById("secondStepCircle").style.backgroundColor =  "#2196F3";

	}
	else
	{
		document.getElementById("nextArrow").style.display = "none";
		document.getElementById("thirdStepCircle").style.backgroundColor =  "#2196F3";

	}
	
    k.next();

}


function backStep()
{
	var k = kontext( document.querySelector( '.kontext' ) );
	if(k.getIndex() === 1)
	{
		document.getElementById("backArrow").style.display = "none";
		document.getElementById("secondStepCircle").style.backgroundColor =  "#9e9e9e";
	}
	else
	{
		document.getElementById("nextArrow").style.display = "block";
		document.getElementById("thirdStepCircle").style.backgroundColor =  "#9e9e9e";

	}
	
    k.prev();

}




function clearPostData()
{
	var table = document.getElementById("tab_logic_post");

	for (var i  = 1; i < table.rows.length; i++) {
		table.deleteRow(i);
	}

}














